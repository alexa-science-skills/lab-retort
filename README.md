# LabRetort #



### What is LabRetort? ###

**LabRetort** is an **Alexa** skill that generates laboratory reports in the form of **Jupyter** notebooks 
as well as immersive audio and visuals via **APL**, **APL for Audio**, **motion and sensing APIs**, and **Alexa Web API for Games**.